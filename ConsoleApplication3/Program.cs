﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;


namespace AgeCalc
{
    class Program
    {
        public static void Main(string[] args)
        {
            int Day=31, Month, Year, CurrentDay, CurrentMonth, CurrentYear, Age;
            Console.WriteLine("A PROGRAM TO CALC AGE");
            Console.WriteLine("\n");
            Console.WriteLine("Enter DD|MM|YYYY of Birth");

            //String[] sepChars = { " ","|" };
            String borndate = Console.ReadLine();

            String[] s = borndate.Split('|');

            int Bornday = Convert.ToInt32(s[0]);
            int BornMonth = Convert.ToInt32(s[1]);
            int BornYear = Convert.ToInt32(s[2]);
            

            //Above for Birth Date, Below for the Current date

            Console.WriteLine("\n");
            Console.WriteLine("Current Date is:");
            Console.WriteLine(DateTime.Now.ToString("dd|MM|yyyy"));
            Console.WriteLine("\n");
            

            //String[] sepChars = { " ","|" };
            String Currentdate = Console.ReadLine();

            String[] c = DateTime.Now.ToString("dd|MM|yyyy").Split('|');

            CurrentDay = Convert.ToInt32(c[0]);
            CurrentMonth = Convert.ToInt32(c[1]);
            CurrentYear = Convert.ToInt32(c[2]);

            //Console.WriteLine("Enter Day of Birth");
            //Console.WriteLine("Day | Month | Year");
            //Day = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("\n");

            //Console.WriteLine("Enter Month of Birth");
            //Console.WriteLine(+Day + " | Month | Year");
            //Month = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("\n");

            //Console.WriteLine("Enter Year of Birth");
            //Console.WriteLine(+Day + "|" + Month + "| Year");
            //Year = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine(+Day + "|" + Month + "|" + Year);
            //Console.WriteLine("\n");



            //Console.WriteLine("Enter Current Day");
            //Console.WriteLine("CurrentDay | CurrentMonth | CurrentYear");
            //CurrentDay = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("\n");

            // Console.WriteLine("Enter Month of Birth");
            //Console.WriteLine(+CurrentDay + " | CurrentMonth | CurrentYear");
            //CurrentMonth = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("\n");

            //Console.WriteLine("Enter Year of Birth");
            //Console.WriteLine(+CurrentDay + "|" + CurrentMonth + "| CurrentYear");
            //CurrentYear = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine(+CurrentDay + "|" + CurrentMonth + "|" + CurrentYear);
            //Console.WriteLine("\n");

            int CalcDay = ((31 - Day) + CurrentDay);
            int CalcMonths = (12 - BornMonth) + CurrentMonth;
            int CalcYear = ((CurrentYear - BornYear) - 1);

            if (CalcDay == 31)
            {
                CalcDay = 0;
                CalcMonths = CalcMonths + 1;
            }

            if (CalcMonths >= 12)
            {
                CalcMonths = CalcMonths%12;
                CalcYear = CalcYear + 1;
            }

            Console.WriteLine("The Current Age is: ");
            Console.Write(+CalcYear + "yrs | " + CalcMonths + "Months | " + CalcDay + "Days");

            //Console.WriteLine("\n");
            Console.ReadKey(true);

        }
    }
}